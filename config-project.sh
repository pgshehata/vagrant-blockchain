#!/usr/bin/env bash
source /home/vagrant/.profile


# MODULES et BDD
	# Activation des modules apache
	sudo a2enmod rewrite
	sudo a2enmod php5
	sudo php5enmod mcrypt

# Mise en place du VHost
VHOST1="mydomain"
VHOST=$(cat <<EOF
<VirtualHost *:80>
	DocumentRoot "/var/www/html/$VHOST1/web/"
	ServerName $VHOST1.local
	
	<Directory "/var/www/html/$VHOST1/web/">
		Options Indexes FollowSymLinks
		AllowOverride All
		Order allow,deny
		Allow from all
		Require all granted
	</Directory>
	
	ErrorLog "/var/log/apache2/$VHOST1.local.error.log"
	CustomLog "/var/log/apache2/$VHOST1.local.log" common
</VirtualHost>
EOF
)
[ -f "/etc/apache2/sites-available/$VHOST1.conf" ] && echo "$VHOST1.conf exist." || sudo echo "${VHOST}" >> "/etc/apache2/sites-available/$VHOST1.conf"
# Activation du site
	sudo a2ensite $VHOST1.conf


	
# Création de la BDD
	mysql -u root -proot -e "CREATE DATABASE IF NOT EXISTS $DBNAME CHARACTER SET utf8 COLLATE utf8_general_ci";

#restart apache
	sudo service apache2 restart