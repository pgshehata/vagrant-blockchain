#!/usr/bin/env bash
source /home/vagrant/.profile
echo "alias ..='cd ..'
alias ...='cd ../..'
alias h='cd ~'
alias c='clear'
alias ll='ls -la'" | tee -a /home/vagrant/.bash_aliases
