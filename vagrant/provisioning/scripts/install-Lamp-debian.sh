#!/usr/bin/env bash
source /home/vagrant/.profile


sudo apt-get update -y
# installation d'apache
sudo apt-get install -y apache2
#  installation de mysql
debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_PASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_PASS"
sudo apt-get install -y  mysql-client mysql-server

# installation de PHP
sudo apt-get install -y php5 libapache2-mod-php5
sudo apt-get install -y php5-common php5-dev php5-cli php5-fpm 
sudo apt-get install -y curl php5-curl php5-gd php5-mcrypt php5-mysql mcrypt