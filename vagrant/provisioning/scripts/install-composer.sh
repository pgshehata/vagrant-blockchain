#!/usr/bin/env bash
source /home/vagrant/.profile

# Install composer
if [ ! -f "/usr/local/bin/composer" ]
then
    curl -s https://getcomposer.org/installer | php
	sudo mv composer.phar /usr/local/bin/composer
	sudo ln -s /usr/local/bin/composer /usr/bin/composer
	export PATH="$HOME/.composer/vendor/bin:$PATH"
fi